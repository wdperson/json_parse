class JSONParse

  def initialize
    get_input
    parse_json
    return_json
  end

  def get_input
    print
    @item_input = STDIN.gets.strip
  end

  def parse_json
    @feature = @item_input[/((\w)*\s(\w)*\s.\s\d{3})/]
    @date_range = @item_input[/(\d{2}\/\d{2}\s.\s\d{2}\/\d{2})/]
    @price = @item_input[/\S{4}\z/]
  end

  def return_json
    puts "{"
    puts "\"feature\": \"#{@feature}\","
    puts "\"date range\": \"#{@date_range}\","
    puts "\"price\": \"#{@price}\""
    puts "}"
  end
end

json_parse = JSONParse.new()
