#!/bin/env ruby
# encoding: utf-8

require 'pry'

#This has to be included for stubbing the IO
class JSONParse
  def get_input
    print
    @item_input = gets.strip
  end

  def parse_json
    @feature = @item_input[/((\w)*\s(\w)*\s.\s\d{3})/]
    @date_range = @item_input[/(\d{2}\/\d{2}\s.\s\d{2}\/\d{2})/]
    @price = @item_input[/\S{4}\z/]
  end
end

describe "#json_parse" do

  it "should get the input from the command line" do
    @json_parse = JSONParse.new
    allow(@json_parse).to receive(:gets) { "$4.99 TXT MESSAGING – 250 09/29 – 10/28 4.99" }
    expect(@json_parse.get_input).to eq("$4.99 TXT MESSAGING – 250 09/29 – 10/28 4.99")
  end

  it "should match another string in the same format" do
    @item_input = "$2.99 PHONE CALLS - 500 11/29 – 12/28 2.99\n"
    @json_parse = JSONParse.new
    @json_parse.parse_json
    expect(@feature).to eq("PHONE CALLS - 500")
  end

  it "should get the feature from the string using a regular expression" do
    @json_parse = JSONParse.new
    allow(@json_parse).to receive(:gets) { "$4.99 TXT MESSAGING – 250 09/29 – 10/28 4.99" }
    @json_parse.parse_json
    expect(@feature).to eq("TXT MESSAGING – 250")
  end

  it "should get the date range from the string" do
    @json_parse = JSONParse.new
    allow(@json_parse).to receive(:item_input) { "$4.99 TXT MESSAGING – 250 09/29 – 10/28 4.99" }
    @json_parse.parse_json
    expect(@date_range).to eq("09/29 - 10/28")
  end

  it "should get the price from the string" do
    @json_parse = JSONParse.new
    allow(@json_parse).to receive(:item_input) { "$4.99 TXT MESSAGING – 250 09/29 – 10/28 4.99" }
    @json_parse.parse_json
    expect(@price).to eq("4.99")
  end

  it "should output a json formatted string" do
  end
end
